package ffufm.macki.api.spec.handler.task.integration

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.spec.handler.user.utils.EntityGenerator
import org.junit.Test
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.post

class TaskTaskHandlerTest: PassTestBase() {

    @Test
    @WithMockUser
    fun `create task should return 201`() {
        val createdUser = userUserRepository.save(EntityGenerator.createUser())
        val createdProject = projectProjectRepository.save(
            EntityGenerator.createProject().copy(owner = createdUser)
        )

        val body = EntityGenerator.createTask()

        //println("/users/${createdUser.id}/projects/${createdProject.id}/tasks/")
        mockMvc.post("/users/${createdUser.id}/projects/${createdProject.id}/tasks/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isCreated() }
        }
    }
}