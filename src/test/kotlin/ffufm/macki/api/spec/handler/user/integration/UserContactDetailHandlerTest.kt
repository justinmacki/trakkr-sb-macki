package ffufm.macki.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.macki.api.PassTestBase
import ffufm.macki.api.repositories.user.UserContactDetailRepository
import ffufm.macki.api.spec.dbo.user.UserContactDetail
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserContactDetailHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var userContactDetailRepository: UserContactDetailRepository


//    @Test
//    @WithMockUser
//    fun `test createContactDetail`() {
//        val body: UserContactDetail = UserContactDetail()
//        val id: Long = 0
//                mockMvc.post("/users/{id}/contact-details/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getById`() {
//        val id: Long = 0
//                mockMvc.get("/contact-details/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test updateContactDetail`() {
//        val body: UserContactDetail = UserContactDetail()
//        val id: Long = 0
//                mockMvc.put("/contact-details/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getByUser`() {
//        val id: Long = 0
//                mockMvc.get("/users/{id}/contact-details/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test remove`() {
//        val id: Long = 0
//                mockMvc.delete("/contact-details/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
}
