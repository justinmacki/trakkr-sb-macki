package ffufm.macki.api.utils

enum class UserTypeEnum(val value: String) {
    GC("GC"),
    SC("SC"),
    CR("CR")
}