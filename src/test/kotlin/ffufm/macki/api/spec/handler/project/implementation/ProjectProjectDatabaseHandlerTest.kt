package ffufm.macki.api.spec.handler.project.implementation

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.repositories.project.ProjectProjectRepository
import ffufm.macki.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.macki.api.spec.handler.user.utils.EntityGenerator
import ffufm.macki.api.utils.Constants
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class ProjectProjectDatabaseHandlerTest : PassTestBase() {

    @Test
    fun `create project should return project`() = runBlocking {
        assertEquals(0, projectProjectRepository.findAll().count())

        val user = EntityGenerator.createUser()
        val createdUser = userUserDatabaseHandler.create(user.toDto())

        val project = EntityGenerator.createProject()
        val createdProject = projectProjectDatabaseHandler.createProject(project.toDto(), createdUser.id!!)

        assertEquals(project.name, createdProject.name)
        assertEquals(project.description, createdProject.description)
        assertEquals(project.status, createdProject.status)
        assertEquals(1, projectProjectRepository.findAll().count())
    }

    @Test
    fun `create project should fail given user Id does not exist`() = runBlocking {
        val project = EntityGenerator.createProject()

        val exception = assertFailsWith<ResponseStatusException> {
            projectProjectDatabaseHandler.createProject(project.toDto(), Constants.INVALID_ID)
        }

        val expectedException = "404 NOT_FOUND \"User with id: ${Constants.INVALID_ID} not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update project should return updated project given valid inputs`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val project = EntityGenerator.createProject().copy( owner =  user.toEntity() )
        val original = projectProjectRepository.save(project)

        val body = original.copy(
            name = "Project XYZ",
            description = "Anything"
        )

        val updatedProject = projectProjectDatabaseHandler.updateProject(body.toDto(), original.id!!)
        assertEquals(body.name, updatedProject.name)
        assertEquals(body.description, updatedProject.description)
    }

    @Test
    fun `update project should throw an error given invalid id`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val project = EntityGenerator.createProject().copy( owner =  user.toEntity() )
        val original = projectProjectRepository.save(project)

        val body = original.copy(
            name = "Project XYZ",
            description = "Anything"
        )

        val exception = assertFailsWith<ResponseStatusException> {
            projectProjectDatabaseHandler.updateProject(body.toDto(), Constants.INVALID_ID)
        }

        val expectedException = "404 NOT_FOUND \"ProjectProject with ID ${Constants.INVALID_ID} not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `delete project should work`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val project = EntityGenerator.createProject().copy( owner =  user.toEntity() )
        val createdProject = projectProjectRepository.save(project)

        assertEquals(1, projectProjectRepository.findAll().count())

        projectProjectDatabaseHandler.removeProject(createdProject.id!!)
        assertEquals(0, projectProjectRepository.findAll().count())
    }

    @Test
    fun `delete project should throw an error given invalid id`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val project = EntityGenerator.createProject().copy( owner =  user.toEntity() )
        projectProjectRepository.save(project)

        val exception = assertFailsWith<ResponseStatusException> {
            projectProjectDatabaseHandler.removeProject(Constants.INVALID_ID)
        }

        val expectedException = "404 NOT_FOUND \"ProjectProject with ID ${Constants.INVALID_ID} not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `getAll projects should return collection of projects`() = runBlocking {
        val project = EntityGenerator.createProject()

        projectProjectRepository.saveAll(
            listOf(
                project,
                project.copy(name = "Try Project", description = "Trial"),
                project.copy(name = "Try Project 1", description = "Trial 1")
            )
        )

        val projects = projectProjectDatabaseHandler.getAllProjects(100,0)
        assertEquals(3, projects.count())
    }

}