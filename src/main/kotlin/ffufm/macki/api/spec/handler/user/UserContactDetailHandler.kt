package ffufm.macki.api.spec.handler.user

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.macki.api.spec.dbo.user.UserContactDetailDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface UserContactDetailDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully added Contact Detail
     */
    suspend fun createContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO

    /**
     * Finds ContactDetails by ID: Returns ContactDetails based on ID
     * HTTP Code 200: The ContactDetail object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): UserContactDetailDTO?

    /**
     * : 
     * HTTP Code 200: Successfully Updated Contact Detail
     */
    suspend fun updateContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO

    /**
     * Find by User: Finds ContactDetails by the parent User id
     * HTTP Code 200: List of ContactDetails items
     */
    suspend fun getByUser(
        id: Long,
        maxResults: Int = 100,
        page: Int = 0
    ): Page<UserContactDetailDTO>

    /**
     * Delete ContactDetail by id.: Deletes one specific ContactDetail.
     */
    suspend fun remove(id: Long)
}

@Controller("user.ContactDetail")
class UserContactDetailHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: UserContactDetailDatabaseHandler

    /**
     * : 
     * HTTP Code 200: Successfully added Contact Detail
     */
    @RequestMapping(value = ["/users/{id:\\d+}/contact-details/"], method = [RequestMethod.POST])
    suspend fun createContactDetail(@RequestBody body: UserContactDetailDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.createContactDetail(body, id) }
    }

    /**
     * Finds ContactDetails by ID: Returns ContactDetails based on ID
     * HTTP Code 200: The ContactDetail object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/contact-details/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * : 
     * HTTP Code 200: Successfully Updated Contact Detail
     */
    @RequestMapping(value = ["/contact-details/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun updateContactDetail(@RequestBody body: UserContactDetailDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.updateContactDetail(body, id) }
    }

    /**
     * Find by User: Finds ContactDetails by the parent User id
     * HTTP Code 200: List of ContactDetails items
     */
    @RequestMapping(value = ["/users/{id:\\d+}/contact-details/"], method = [RequestMethod.GET])
    suspend fun getByUser(
        @PathVariable("id") id: Long,
        @RequestParam("maxResults") maxResults: Int? = 100,
        @RequestParam("page") page: Int? = 0
    ): ResponseEntity<*> {

        return paging { databaseHandler.getByUser(id, maxResults ?: 100, page ?: 0) }
    }

    /**
     * Delete ContactDetail by id.: Deletes one specific ContactDetail.
     */
    @RequestMapping(value = ["/contact-details/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }
}
