package ffufm.macki.api.repositories.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.macki.api.spec.dbo.user.UserAddress
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserAddressRepository : PassRepository<UserAddress, Long> {
    @Query(
        "SELECT t from UserAddress t LEFT JOIN FETCH t.user",
        countQuery = "SELECT count(id) FROM UserAddress"
    )
    fun findAllAndFetchUser(pageable: Pageable): Page<UserAddress>

    @Query(
        """
                SELECT CASE WHEN COUNT(a) > 0 THEN TRUE ELSE FALSE END
                FROM UserAddress a WHERE a.street = :street AND a.barangay = :barangay AND 
                a.city = :city AND a.province = :province AND a.zipCode = :zipCode
        """
    )
    fun doesAddressExist(street: String, barangay: String, city: String, province: String, zipCode: String, ): Boolean
}
