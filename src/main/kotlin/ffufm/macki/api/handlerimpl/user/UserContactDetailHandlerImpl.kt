package ffufm.macki.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.macki.api.repositories.user.UserContactDetailRepository
import ffufm.macki.api.repositories.user.UserUserRepository
import ffufm.macki.api.spec.dbo.user.UserContactDetail
import ffufm.macki.api.spec.dbo.user.UserContactDetailDTO
import ffufm.macki.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.macki.api.spec.handler.user.UserUserHandler
import ffufm.macki.api.utils.Constants
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserContactDetailHandler")
class UserContactDetailHandlerImpl (
    private val userRepository: UserUserRepository
        ): PassDatabaseHandler<UserContactDetail,
        UserContactDetailRepository>(), UserContactDetailDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully added Contact Detail
     */
    override suspend fun createContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO {
        val bodyEntity = body.toEntity()
        val user = userRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id not found")
        }

        if(repository.doesContactDetailExist(bodyEntity.contactDetails)){
            throw ResponseStatusException(HttpStatus.CONFLICT, "Contact Details ${bodyEntity.contactDetails} already exists")
        }

        if(repository.countContactDetailsByUserId(id) >= Constants.MAX_CONTACT_DETAILS){
            throw ResponseStatusException (HttpStatus.BAD_REQUEST, "You cannot create more than four details.")
        }

        if(bodyEntity.isPrimary){
            // Create a query that will update the value of all contactDetails isPrimary=false
            repository.updateIsPrimaryToFalse(id)
        }

        return repository.save(bodyEntity.copy( user = user )).toDto()
    }

    /**
     * Finds ContactDetails by ID: Returns ContactDetails based on ID
     * HTTP Code 200: The ContactDetail object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): UserContactDetailDTO? {

        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * : 
     * HTTP Code 200: Successfully Updated Contact Detail
     */
    override suspend fun updateContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO {
        val bodyEntity = body.toEntity()
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(original.copy(
            contactDetails = bodyEntity.contactDetails,
            contactType = bodyEntity.contactType
        )).toDto()
    }

    /**
     * Find by User: Finds ContactDetails by the parent User id
     * HTTP Code 200: List of ContactDetails items
     */
    override suspend fun getByUser(
        id: Long,
        maxResults: Int,
        page: Int
    ): Page<UserContactDetailDTO> {
        val pagination = PageRequest.of(page ?: 0, maxResults ?: 100)
        return repository.findAll(pagination).toDtos()
    }

    /**
     * Delete ContactDetail by id.: Deletes one specific ContactDetail.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)
    }
}
