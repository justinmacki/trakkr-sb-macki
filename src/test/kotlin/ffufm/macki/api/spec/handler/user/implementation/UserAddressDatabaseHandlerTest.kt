package ffufm.macki.api.spec.handler.user.implementation

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.repositories.user.UserAddressRepository
import ffufm.macki.api.spec.dbo.user.UserAddress
import ffufm.macki.api.spec.handler.user.UserAddressDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class UserAddressDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var userAddressRepository: UserAddressRepository

    @Autowired
    lateinit var userAddressDatabaseHandler: UserAddressDatabaseHandler

    @Test
    fun `test createAddress`() = runBlocking {
        val body: UserAddress = UserAddress()
        val id: Long = 0
        userAddressDatabaseHandler.createAddress(body.toDto(), id)
        Unit
    }

    @Test
    fun `test getById`() = runBlocking {
        val id: Long = 0
        userAddressDatabaseHandler.getById(id)
        Unit
    }

    @Test
    fun `test updateAddress`() = runBlocking {
        val body: UserAddress = UserAddress()
        val id: Long = 0
        userAddressDatabaseHandler.updateAddress(body.toDto(), id)
        Unit
    }

    @Test
    fun `test remove`() = runBlocking {
        val id: Long = 0
        userAddressDatabaseHandler.remove(id)
        Unit
    }

    @Test
    fun `test getByUser`() = runBlocking {
        val id: Long = 0
        val maxResults: Int = 100
        val page: Int = 0
        userAddressDatabaseHandler.getByUser(id, maxResults, page)
        Unit
    }
}
