package ffufm.macki.api.spec.handler.user

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.macki.api.spec.dbo.user.UserAddressDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface UserAddressDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully Added Address
     */
    suspend fun createAddress(body: UserAddressDTO, id: Long): UserAddressDTO

    /**
     * Finds Addresses by ID: Returns Addresses based on ID
     * HTTP Code 200: The Address object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): UserAddressDTO?

    /**
     * : 
     * HTTP Code 200: Successfully Updated Address
     */
    suspend fun updateAddress(body: UserAddressDTO, id: Long): UserAddressDTO

    /**
     * Delete Address by id.: Deletes one specific Address.
     */
    suspend fun remove(id: Long)

    /**
     * Find by User: Finds Addresses by the parent User id
     * HTTP Code 200: List of Addresses items
     */
    suspend fun getByUser(
        id: Long,
        maxResults: Int = 100,
        page: Int = 0
    ): Page<UserAddressDTO>
}

@Controller("user.Address")
class UserAddressHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: UserAddressDatabaseHandler

    /**
     * : 
     * HTTP Code 200: Successfully Added Address
     */
    @RequestMapping(value = ["/users/{id:\\d+}/addresses/"], method = [RequestMethod.POST])
    suspend fun createAddress(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.createAddress(body, id) }
    }

    /**
     * Finds Addresses by ID: Returns Addresses based on ID
     * HTTP Code 200: The Address object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/addresses/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * : 
     * HTTP Code 200: Successfully Updated Address
     */
    @RequestMapping(value = ["/addresses/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun updateAddress(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.updateAddress(body, id) }
    }

    /**
     * Delete Address by id.: Deletes one specific Address.
     */
    @RequestMapping(value = ["/addresses/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Find by User: Finds Addresses by the parent User id
     * HTTP Code 200: List of Addresses items
     */
    @RequestMapping(value = ["/users/{id:\\d+}/addresses/"], method = [RequestMethod.GET])
    suspend fun getByUser(
        @PathVariable("id") id: Long,
        @RequestParam("maxResults") maxResults: Int? = 100,
        @RequestParam("page") page: Int? = 0
    ): ResponseEntity<*> {

        return paging { databaseHandler.getByUser(id, maxResults ?: 100, page ?: 0) }
    }
}
