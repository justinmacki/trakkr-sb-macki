package ffufm.macki.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.macki.api.PassTestBase
import ffufm.macki.api.repositories.user.UserAddressRepository
import ffufm.macki.api.spec.dbo.user.UserAddress
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserAddressHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var userAddressRepository: UserAddressRepository


//    @Test
//    @WithMockUser
//    fun `test createAddress`() {
//        val body: UserAddress = UserAddress()
//        val id: Long = 0
//                mockMvc.post("/users/{id}/addresses/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }

//    @Test
//    @WithMockUser
//    fun `test getById`() {
//        val id: Long = 0
//                mockMvc.get("/addresses/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test updateAddress`() {
//        val body: UserAddress = UserAddress()
//        val id: Long = 0
//                mockMvc.put("/addresses/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test remove`() {
//        val id: Long = 0
//                mockMvc.delete("/addresses/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getByUser`() {
//        val id: Long = 0
//                mockMvc.get("/users/{id}/addresses/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
}
