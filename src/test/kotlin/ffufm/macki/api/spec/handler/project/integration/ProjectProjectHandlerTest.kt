package ffufm.macki.api.spec.handler.project.integration

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.spec.dbo.user.UserUser
import ffufm.macki.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class ProjectProjectHandlerTest : PassTestBase() {

    @Test
    @WithMockUser
    fun `create project should return 200`() {
        val createdUser = userUserRepository.save(EntityGenerator.createUser())

        val body = EntityGenerator.createProject()

        mockMvc.post("/users/{id}/projects/", createdUser.id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `create project should return 404 given invalid owner id`() {
        val invalidId: Long = -1

        val body = EntityGenerator.createProject()

        mockMvc.post("/users/{id}/projects/", invalidId) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isNotFound() }
        }
    }

    @Test
    @WithMockUser
    fun `update should return 200`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val project = EntityGenerator.createProject().copy( owner =  user )
        val original = projectProjectRepository.save(project)

        val body = original.copy(
            name = "Project XYZ",
            description = "Anything"
        )

        mockMvc.put("/projects/{id}/", original.id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `update should return 404 given invalid id`() {
        val invalidId: Long = -1

        val body = EntityGenerator.createProject().copy(
            name = "Project XYZ",
            description = "Anything"
        )

        mockMvc.put("/projects/{id}/", invalidId) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isNotFound() }
        }
    }

    @Test
    @WithMockUser
    fun `delete project should return 200`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val project = EntityGenerator.createProject().copy( owner =  user )
        val createdProject = projectProjectRepository.save(project)

        mockMvc.delete("/projects/{id}/", createdProject.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `delete project should return 404 given invalid id`() {
        val invalidId: Long = 687580

        mockMvc.delete("/projects/{id}/", invalidId) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isNotFound() }
        }
    }

    @Test
    @WithMockUser
    fun `getAll projects should have correct pagination`() {
        val project = EntityGenerator.createProject()

        val projects = projectProjectRepository.saveAll(
            listOf(
                project,
                project.copy(name = "Try Project", description = "Trial"),
                project.copy(name = "Try Project 1", description = "Trial 1")
            )
        )

        val page = 0
        val maxResult = 2
        val totalPages = projects.count() / maxResult
        mockMvc.get("/projects/?page=$page&maxResults=$maxResult") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }
}