package ffufm.macki.api.spec.handler.task.implementation

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.repositories.task.TaskTaskRepository
import ffufm.macki.api.spec.handler.task.TaskTaskDatabaseHandler
import ffufm.macki.api.spec.handler.user.utils.EntityGenerator
import ffufm.macki.api.utils.Constants
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TaskTaskDatabaseHandlerTest : PassTestBase() {

    @Test
    fun `create task should return task`() = runBlocking {
        assertEquals(0, taskTaskRepository.findAll().count())

        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())

        val project = projectProjectDatabaseHandler.createProject(EntityGenerator
            .createProject()
            .toDto(), user.id!!)

        val task = EntityGenerator.createTask()
        val createdTask = taskTaskDatabaseHandler.create(task.toDto(), user.id!!, project.id!!)

        assertEquals(task.name, createdTask.name)
        assertEquals(task.description, createdTask.description)
        assertEquals(task.status, createdTask.status)
        assertEquals(1, taskTaskRepository.findAll().count())
    }

    @Test
    fun `create task should fail given user Id does not exist`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())

        val project = projectProjectDatabaseHandler.createProject(EntityGenerator
            .createProject()
            .toDto(), user.id!!)

        val task = EntityGenerator.createTask()

        val exception = assertFailsWith<ResponseStatusException> {
            taskTaskDatabaseHandler.create(task.toDto(), Constants.INVALID_ID, project.id!!)
        }

        val expectedException = "404 NOT_FOUND \"User with id: ${Constants.INVALID_ID} not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create task should fail given project Id does not exist`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())

        val task = EntityGenerator.createTask()

        val exception = assertFailsWith<ResponseStatusException> {
            taskTaskDatabaseHandler.create(task.toDto(), user.id!!, Constants.INVALID_ID)
        }

        val expectedException = "404 NOT_FOUND \"Project with id: ${Constants.INVALID_ID} not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update task should return updated task given valid inputs`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val project = projectProjectDatabaseHandler.createProject(EntityGenerator.createProject().toDto(), user.id!!)
        val task = EntityGenerator.createTask().copy(
            assignee =  user.toEntity(),
            taskProjectRelationship = project.toEntity()
        )
        val original = taskTaskRepository.save(task)

        val body = original.copy(
            name = "Task XYZ",
            description = "Something",
            status = "DONE"
        )

        val updatedTask = taskTaskDatabaseHandler.update(body.toDto(), original.id!!)
        assertEquals(body.name, updatedTask.name)
        assertEquals(body.description, updatedTask.description)
        assertEquals(body.status, updatedTask.status)
    }

    @Test
    fun `update task should throw an error given invalid id`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val project = projectProjectDatabaseHandler.createProject(EntityGenerator.createProject().toDto(), user.id!!)
        val task = EntityGenerator.createTask().copy(
            assignee =  user.toEntity(),
            taskProjectRelationship = project.toEntity()
        )
        val original = taskTaskRepository.save(task)

        val body = original.copy(
            name = "Task XYZ",
            description = "Something",
            status = "DONE"
        )

        val exception = assertFailsWith<ResponseStatusException> {
            taskTaskDatabaseHandler.update(body.toDto(), Constants.INVALID_ID)
        }

        val expectedException = "404 NOT_FOUND \"TaskTask with ID ${Constants.INVALID_ID} not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `delete task should work`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val project = projectProjectDatabaseHandler.createProject(EntityGenerator.createProject().toDto(), user.id!!)
        val task = taskTaskRepository.save(EntityGenerator.createTask().copy(
            assignee =  user.toEntity(),
            taskProjectRelationship = project.toEntity()
        ))

        assertEquals(1, taskTaskRepository.findAll().count())

        taskTaskDatabaseHandler.remove(task.id!!)
        assertEquals(0, taskTaskRepository.findAll().count())
    }

    @Test
    fun `delete task should throw an error given invalid id`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val project = projectProjectDatabaseHandler.createProject(EntityGenerator.createProject().toDto(), user.id!!)
        taskTaskRepository.save(EntityGenerator.createTask().copy(
            assignee =  user.toEntity(),
            taskProjectRelationship = project.toEntity()
        ))

        val exception = assertFailsWith<ResponseStatusException> {
            taskTaskDatabaseHandler.remove(Constants.INVALID_ID)
        }

        val expectedException = "404 NOT_FOUND \"TaskTask with ID ${Constants.INVALID_ID} not found\""

        assertEquals(expectedException, exception.message)
    }
}