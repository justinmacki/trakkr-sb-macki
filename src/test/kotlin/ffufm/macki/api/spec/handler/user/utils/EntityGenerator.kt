package ffufm.macki.api.spec.handler.user.utils

import ffufm.macki.api.spec.dbo.project.ProjectProject
import ffufm.macki.api.spec.dbo.task.TaskTask
import ffufm.macki.api.spec.dbo.user.UserAddress
import ffufm.macki.api.spec.dbo.user.UserContactDetail
import ffufm.macki.api.spec.dbo.user.UserUser
import ffufm.macki.api.utils.UserTypeEnum

object EntityGenerator {
    fun createUser(): UserUser = UserUser(
        firstName = "Brandon",
        lastName = "Cruz",
        email = "brandon@brandon.com",
        userType = UserTypeEnum.CR.value
    )

    fun createContactDetail(): UserContactDetail = UserContactDetail(
        contactDetails = "09773091979",
        contactType = "Mobile Phone"
    )

    fun createAddress(): UserAddress = UserAddress(
        street = "Hello Street",
        barangay = "World",
        city = "Valenzuela",
        province = "Metro Manila",
        zipCode = "1028"
    )

    fun createProject(): ProjectProject = ProjectProject(
        name = "Project Z",
        description = "A project about something",
        status = "ON-GOING"
    )

    fun createTask(): TaskTask = TaskTask(
        name = "Task Sample 1",
        description = "Task Description Sample 1",
        status = "ON-GOING"
    )
}