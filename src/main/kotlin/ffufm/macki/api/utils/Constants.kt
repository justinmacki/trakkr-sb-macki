package ffufm.macki.api.utils

object Constants {
    const val INVALID_ID: Long = -1

    const val MAX_CONTACT_DETAILS = 3
}