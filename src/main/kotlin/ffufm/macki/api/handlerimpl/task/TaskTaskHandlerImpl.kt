package ffufm.macki.api.handlerimpl.task

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.macki.api.repositories.project.ProjectProjectRepository
import ffufm.macki.api.repositories.task.TaskTaskRepository
import ffufm.macki.api.repositories.user.UserUserRepository
import ffufm.macki.api.spec.dbo.task.TaskTask
import ffufm.macki.api.spec.dbo.task.TaskTaskDTO
import ffufm.macki.api.spec.handler.task.TaskTaskDatabaseHandler
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("task.TaskTaskHandler")
class TaskTaskHandlerImpl(
    private val userRepository: UserUserRepository,
    private val projectRepository: ProjectProjectRepository
) : PassDatabaseHandler<TaskTask, TaskTaskRepository>(),
    TaskTaskDatabaseHandler {

    override suspend fun create(body: TaskTaskDTO, id: Long, projectId: Long): TaskTaskDTO {
        val bodyEntity = body.toEntity()

        val user = userRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id not found")
        }

        val project = projectRepository.findById(projectId).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Project with id: $projectId not found")
        }

        return repository.save(bodyEntity.copy(
            assignee = user,
            taskProjectRelationship = project
        )).toDto()
    }

    override suspend fun update(body: TaskTaskDTO, id: Long): TaskTaskDTO {
        val bodyEntity = body.toEntity()
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(original.copy(
            name = bodyEntity.name,
            description = bodyEntity.description,
            status = bodyEntity.status
        )).toDto()
    }

    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)
    }
}