package ffufm.macki.api.repositories.project

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.macki.api.spec.dbo.project.ProjectProject
import org.springframework.stereotype.Repository

@Repository
interface ProjectProjectRepository : PassRepository<ProjectProject, Long>
