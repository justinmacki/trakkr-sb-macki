package ffufm.macki.api.repositories.task

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.macki.api.spec.dbo.task.TaskTask
import org.springframework.stereotype.Repository

@Repository
interface TaskTaskRepository : PassRepository<TaskTask, Long>