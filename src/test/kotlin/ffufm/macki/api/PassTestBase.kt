package ffufm.macki.api

import com.fasterxml.jackson.databind.ObjectMapper
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.security.SpringSecurityAuditorAware
import ffufm.macki.api.repositories.project.ProjectProjectRepository
import ffufm.macki.api.repositories.task.TaskTaskRepository
import ffufm.macki.api.repositories.user.UserUserRepository
import ffufm.macki.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.macki.api.spec.handler.task.TaskTaskDatabaseHandler
import ffufm.macki.api.spec.handler.user.UserUserDatabaseHandler
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [SBMacki::class, SpringSecurityAuditorAware::class])
@AutoConfigureMockMvc
abstract class PassTestBase {

    @Autowired
    lateinit var userUserRepository: UserUserRepository

    @Autowired
    lateinit var userUserDatabaseHandler: UserUserDatabaseHandler

    @Autowired
    lateinit var  projectProjectRepository: ProjectProjectRepository

    @Autowired
    lateinit var projectProjectDatabaseHandler: ProjectProjectDatabaseHandler

    @Autowired
    lateinit var taskTaskRepository: TaskTaskRepository

    @Autowired
    lateinit var taskTaskDatabaseHandler: TaskTaskDatabaseHandler

    @Autowired
    lateinit var context: ApplicationContext

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @Autowired
    lateinit var mockMvc: MockMvc

    @Before
    fun initializeContext() {
        SpringContext.context = context
    }

    @Before
    fun cleanRepositories() {
        userUserRepository.deleteAll()
    }
}
