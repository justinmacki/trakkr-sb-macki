package ffufm.macki.api.spec.handler.user.implementation

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.repositories.user.UserContactDetailRepository
import ffufm.macki.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.macki.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UserContactDetailDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var userContactDetailRepository: UserContactDetailRepository

    @Autowired
    lateinit var userContactDetailDatabaseHandler: UserContactDetailDatabaseHandler

    @Test
    fun `create contact should return contact detail`() = runBlocking {
        assertEquals(0, userContactDetailRepository.findAll().count())

        val user = EntityGenerator.createUser()
        val createdUser = userUserDatabaseHandler.create(user.toDto())

        val contactDetail = EntityGenerator.createContactDetail()
        val createdContactDetail = userContactDetailDatabaseHandler.createContactDetail(
            contactDetail.toDto(), createdUser.id!!)

        assertEquals(contactDetail.contactDetails, createdContactDetail.contactDetails)
        assertEquals(contactDetail.contactType, createdContactDetail.contactType)
        assertEquals(1, userContactDetailRepository.findAll().count())
    }

    @Test
    fun `create contact should fail given user Id does not exist`() = runBlocking {
        val invalidId: Long = 456
        val contactDetail = EntityGenerator.createContactDetail()

        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.createContactDetail(contactDetail.toDto(), invalidId)
        }

        val expectedException = "404 NOT_FOUND \"User with id: $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create contact should fail given duplicate contact detail`() = runBlocking {
        val user = EntityGenerator.createUser()
        val createdUser = userUserDatabaseHandler.create(user.toDto())

        val contactDetail = EntityGenerator.createContactDetail().copy( user = createdUser.toEntity() )
        userContactDetailRepository.save(contactDetail)

        val duplicateContactDetail = contactDetail.copy(
            contactType = "Telephone"
        )

        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.createContactDetail(duplicateContactDetail.toDto(), createdUser.id!!)
        }

        val expectedException = "409 CONFLICT \"Contact Details ${
            duplicateContactDetail.contactDetails} already exists\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create contactDetail should return correct isPrimary value`() = runBlocking {
        // 1. Create a contact detail (isPrimary = true)
        val user = EntityGenerator.createUser()
        val savedUser = userUserDatabaseHandler.create(user.toDto())

        val bodyContactDetail =  EntityGenerator.createContactDetail()
        val firstContactDetail = userContactDetailRepository.save(bodyContactDetail.copy(
            user = savedUser.toEntity()
        ))
        assertEquals(true, firstContactDetail.isPrimary)

        // 2. Create another contactDetail (isPrimary =  missing)
        // Test if the second contactDetail (isPrimary = false)
        val secondContactDetail = userContactDetailDatabaseHandler.createContactDetail(bodyContactDetail.copy(
            contactDetails = "09127183333",
            isPrimary = false
        ).toDto(), savedUser.id!!)
        assertEquals(false, secondContactDetail.isPrimary)

        // 3. Create another contactDetail (isPrimary = true)
        val thirdContactDetail = userContactDetailDatabaseHandler.createContactDetail(bodyContactDetail.copy(
            contactDetails = "090071433333",
            isPrimary = true
        ).toDto(), savedUser.id!!)
        assertEquals(true, thirdContactDetail.isPrimary)
        assertEquals(false, userContactDetailRepository.findById(firstContactDetail.id!!).get().isPrimary)
    }

    @Test
    fun `create contactDetails should throw an error given 4th contactDetail`() = runBlocking {
        //save three contactDetails
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())

        val body = EntityGenerator.createContactDetail().copy(user = user.toEntity())
        userContactDetailRepository.saveAll(
            listOf(
                body,
                body.copy(contactDetails = "092329834444", isPrimary = false),
                body.copy(contactDetails = "095459834444", isPrimary = false)
            )
        )

        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.createContactDetail(body.copy(
                contactDetails = "0912345678",
                isPrimary = false
            ).toDto(), user.id!!)
        }

        val expectedException = "400 BAD_REQUEST \"You cannot create more than four details.\""

        assertEquals(expectedException,exception.message)
    }

    @Test
    fun `get contact detail should return contact detail given valid id`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val contactDetail = EntityGenerator.createContactDetail().copy(
            user = user.toEntity()
        )
        val createdContactDetail = userContactDetailRepository.save(contactDetail)

        val contactDetailFromServiceImpl =  userContactDetailDatabaseHandler.getById(createdContactDetail.id!!)
        assertEquals(createdContactDetail.contactDetails, contactDetailFromServiceImpl!!.contactDetails)
        assertEquals(createdContactDetail.contactType, contactDetailFromServiceImpl!!.contactType)
    }

    @Test
    fun `get contact detail should throw an error given invalid id`() = runBlocking {
        val invalidId: Long = -1

        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.getById(invalidId)
        }

        val expectedException = "404 NOT_FOUND \"UserContactDetail with ID $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update contact detail should return updated contact detail given valid inputs`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val contactDetail = EntityGenerator.createContactDetail().copy( user =  user.toEntity() )
        val original = userContactDetailRepository.save(contactDetail)

        val body = original.copy(
            contactDetails = "0999-9999-999",
            contactType = "Telephone"
        )

        val updatedContactDetail = userContactDetailDatabaseHandler.updateContactDetail(body.toDto(), original.id!!)
        assertEquals(body.contactDetails, updatedContactDetail.contactDetails)
        assertEquals(body.contactType, updatedContactDetail.contactType)
    }

    @Test
    fun `update contact detail should throw an error given invalid id`() = runBlocking {
        val invalidId: Long = -1
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val contactDetail = EntityGenerator.createContactDetail().copy( user =  user.toEntity() )
        val original = userContactDetailRepository.save(contactDetail)

        val body = original.copy(
            contactDetails = "0999-9999-999",
            contactType = "Telephone"
        )

        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.updateContactDetail(body.toDto(), invalidId)
        }

        val expectedException = "404 NOT_FOUND \"UserContactDetail with ID $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `delete contact detail should work`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val contactDetail = EntityGenerator.createContactDetail().copy( user =  user.toEntity() )
        val createdContactDetail = userContactDetailRepository.save(contactDetail)

        assertEquals(1, userContactDetailRepository.findAll().count())

        userContactDetailDatabaseHandler.remove(createdContactDetail.id!!)
        assertEquals(0, userContactDetailRepository.findAll().count())
    }

    @Test
    fun `delete contact detail should throw an error given invalid id`() = runBlocking {
        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())
        val invalidId: Long = -1
        val contactDetail = EntityGenerator.createContactDetail().copy( user =  user.toEntity() )
        userContactDetailRepository.save(contactDetail)

        assertEquals(1, userContactDetailRepository.findAll().count())

        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.remove(invalidId)
        }

        val expectedException = "404 NOT_FOUND \"UserContactDetail with ID $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    // userUserDatabaseHandler
    // userContactDetailRepository
    // userContactDetailDatabaseHandler

}
