package ffufm.macki.api.spec.handler.user.implementation

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.spec.dbo.user.UserUser
import ffufm.macki.api.spec.handler.user.UserUserDatabaseHandler
import ffufm.macki.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UserUserDatabaseHandlerTest : PassTestBase() {

    @Test
    fun `getById should return a user`() = runBlocking {
        val user = UserUser(
            firstName = " Brandon",
            lastName = "Cruz",
            email = "brandon@brandon.com",
            userType = "GC"
        )

        val savedUser = userUserRepository.save(user)

        userUserDatabaseHandler.getById(savedUser.id!!)

        assertEquals(1, userUserRepository.findAll().size)
    }

    @Test
    fun `get user should throw an error given invalid id`() = runBlocking {
        val invalidId: Long = 123

        val exception = assertFailsWith<ResponseStatusException> {
            userUserDatabaseHandler.getById(invalidId)
        }

        val expectedException = "404 NOT_FOUND \"UserUser with ID $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `getAll users should return collection of users`() = runBlocking {
        val user = EntityGenerator.createUser()

        userUserRepository.saveAll(
            listOf(
                user,
                user.copy(email = "Hello@gmail.com", lastName = "World"),
                user.copy(email = "Hahaha@haha.com", lastName = "Hehehhe")
            )
        )

        val users = userUserDatabaseHandler.getAll(100,0)
        assertEquals(3, users.count())
    }

    @Test
    fun `create should return user`() = runBlocking {
        val user = EntityGenerator.createUser()

        userUserDatabaseHandler.create(user.toDto())

        assertEquals(1, userUserRepository.findAll().count())
    }

    @Test
    fun `update user should return updated user given valid inputs`() = runBlocking {
        val user = EntityGenerator.createUser()
        val original = userUserRepository.save(user)

        val body = original.copy(
            firstName = "Brenda",
            lastName = "Mage",
            email = "brenda@brenda.com"
        )

        val updatedUser = userUserDatabaseHandler.update(body.toDto(), original.id!!)
        assertEquals(body.firstName, updatedUser.firstName)
        assertEquals(body.lastName, updatedUser.lastName)
        assertEquals(body.email, updatedUser.email)
    }

    @Test
    fun `remove user should work`() = runBlocking {
        val user = EntityGenerator.createUser()
        val createdUser = userUserRepository.save(user)

        assertEquals(1, userUserRepository.findAll().count())

        userUserDatabaseHandler.remove(createdUser.id!!)
        assertEquals(0, userUserRepository.findAll().count())
    }
}
