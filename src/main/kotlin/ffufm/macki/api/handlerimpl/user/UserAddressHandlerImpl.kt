package ffufm.macki.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.macki.api.repositories.user.UserAddressRepository
import ffufm.macki.api.repositories.user.UserUserRepository
import ffufm.macki.api.spec.dbo.user.UserAddress
import ffufm.macki.api.spec.dbo.user.UserAddressDTO
import ffufm.macki.api.spec.handler.user.UserAddressDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserAddressHandler")
class UserAddressHandlerImpl (
    private val userRepository: UserUserRepository
        ): PassDatabaseHandler<UserAddress, UserAddressRepository>(),
        UserAddressDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully Added Address
     */
    override suspend fun createAddress(body: UserAddressDTO, id: Long): UserAddressDTO {
        val bodyEntity = body.toEntity()
        val user = userRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id not found")
        }

        if(repository.doesAddressExist(bodyEntity.street, bodyEntity.barangay,
                bodyEntity.city, bodyEntity.province, bodyEntity.zipCode)){
            throw ResponseStatusException(HttpStatus.CONFLICT,
                "Address ${body.street}, ${body.barangay}, ${body.city}, ${body.province}, ${body.zipCode}, " +
                        "already exists")
        }

        return repository.save(bodyEntity.copy( user = user )).toDto()
    }

    /**
     * Finds Addresses by ID: Returns Addresses based on ID
     * HTTP Code 200: The Address object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): UserAddressDTO? {

        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * : 
     * HTTP Code 200: Successfully Updated Address
     */
    override suspend fun updateAddress(body: UserAddressDTO, id: Long): UserAddressDTO {
        val bodyEntity = body.toEntity()
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(original.copy(
            street = bodyEntity.street,
            barangay = bodyEntity.barangay,
            city = bodyEntity.city,
            province = bodyEntity.province,
            zipCode = bodyEntity.zipCode
        )).toDto()
    }

    /**
     * Delete Address by id.: Deletes one specific Address.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)
    }

    /**
     * Find by User: Finds Addresses by the parent User id
     * HTTP Code 200: List of Addresses items
     */
    override suspend fun getByUser(
        id: Long,
        maxResults: Int,
        page: Int
    ): Page<UserAddressDTO> {
        val pagination = PageRequest.of(page ?: 0, maxResults ?: 100)
        return repository.findAll(pagination).toDtos()
    }
}
