package ffufm.macki.api.spec.handler.user.integration

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.spec.dbo.user.UserUser
import ffufm.macki.api.spec.handler.user.utils.EntityGenerator
import ffufm.macki.api.utils.UserTypeEnum
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserUserHandlerTest : PassTestBase() {

    @Test
    @WithMockUser
    fun `getById should return isOk`() {

        val body = UserUser(
            firstName = " Brandon",
            lastName = "Cruz",
            email = "brandon@brandon.com",
            userType = "GC"
        )

        val user = userUserRepository.save(body)

        mockMvc.get("/users/{id}/", user.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    /**
     * NOT REALLY SURE WITH THIS ONE
     */
    @Test
    @WithMockUser
    fun `getAll users should have correct pagination`() {
        val body = EntityGenerator.createUser()

        val users = userUserRepository.saveAll(
            listOf(
                body,
                body.copy(email = "brenda@gmail.com"),
                body.copy(email = "brend0@gmail.com"),
                body.copy(email = "bren@gmail.com"),
                body.copy(email = "bro@gmail.com"),
                body.copy(email = "brandy@gmail.com")
            )
        )

        val page = 0
        val maxResult = 2
        val totalPages = users.count() / maxResult
        mockMvc.get("/users/?page=$page&maxResults=$maxResult") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `create user should return 200`() {
        val body = EntityGenerator.createUser()
        mockMvc.post("/users/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `update user should return 200`() {
        val user = userUserRepository.save( EntityGenerator.createUser() )

        val updatedUserBody = user.copy(
            firstName = "Brenda",
            lastName = "Mage",
            email = "brandon@brandon.com",
            userType = UserTypeEnum.GC.value
        )

        mockMvc.put("/users/{id}/", user.id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(updatedUserBody)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `update user should return 404 given invalid user id`() {
        val invalidId: Long = -1

        val body = EntityGenerator.createUser().copy(lastName = "Lopez")

        mockMvc.put("/users/{id}/", invalidId) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isNotFound() }
        }
    }

    @Test
    @WithMockUser
    fun `delete user should return 200 given valid userId`() {
        val user = userUserRepository.save( EntityGenerator.createUser() )

        mockMvc.delete("/users/{id}/", user.id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `delete user should return 404 given invalid userId`() {
        val invalidId = -1

        mockMvc.delete("/users/{id}/", invalidId) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }
}
